#include "DataBaseAccess.h"

#include <iostream>
#include <map>

int albumCallback(void* data, int argc, char** argv, char** azColName);
int userCallback(void* data, int argc, char** argv, char** azColName);
int pictureCallback(void* data, int argc, char** argv, char** azColName);
int tagCallback(void* data, int argc, char** argv, char** azColName);

DataBaseAccess::DataBaseAccess()
{
	_db = nullptr;
}

void DataBaseAccess::createAlbum(const Album& album)
{
	const std::string sqlStastement = "INSERT INTO ALBUMS(NAME, USER_ID, CREATION_DATE) VALUES('" + album.getName() +
		"'," + std::to_string(album.getOwnerId()) + ",'" + album.getCreationDate() +  "'); ";
	exec_sql_statement(sqlStastement);
}

bool DataBaseAccess::doesAlbumExists(const std::string& albumName, int userId)
{
	const std::string sqlStatement = "SELECT * FROM ALBUMS WHERE NAME='" + albumName + "' AND USER_ID=" + std::to_string(userId) +";";
	const std::list<Album> allAlbums = exec_albums_select(sqlStatement);

	return allAlbums.size() != 0;
}

Album DataBaseAccess::openAlbum(const std::string& albumName)
{
	const std::string sqlStatement = "SELECT * FROM ALBUMS WHERE NAME='" + albumName + "'";
	const std::list<Album> allAlbums = exec_albums_select(sqlStatement);

	if (allAlbums.size() == 0)
		throw std::exception("ERROR: Can't find album\n");
	
	Album album = Album(allAlbums.front());

	return album;
}

std::list<Album> DataBaseAccess::exec_albums_select(const std::string& sqlStatement)
{
	std::list<Album> allAlbums = std::list<Album>();
	char* errMessage = nullptr;
	const int res = sqlite3_exec(_db, sqlStatement.c_str(), albumCallback, &allAlbums, &errMessage);
	if (res != SQLITE_OK) {
		std::string temp = errMessage;

		delete errMessage;
		throw std::exception(temp.c_str());
	}

	for (Album& album : allAlbums)
		addPicturesToAlbum(album);
	
	return allAlbums;
}

std::list<User> DataBaseAccess::exec_users_select(const std::string& sqlStatement)
{
	std::list<User> allUsers = std::list<User>();
	char* errMessage = nullptr;
	const int res = sqlite3_exec(_db, sqlStatement.c_str(), &userCallback, &allUsers, &errMessage);
	if (res != SQLITE_OK) {
		std::string temp = errMessage;

		delete errMessage;
		throw std::exception(temp.c_str());
	}

	return allUsers;
}

const std::list<Album> DataBaseAccess::getAlbums()
{
	const std::string sqlStatement = "SELECT * FROM ALBUMS;";
	return exec_albums_select(sqlStatement);
}

const std::list<Album> DataBaseAccess::getAlbumsOfUser(const User& user)
{
	const std::string sqlStatement = "SELECT * FROM ALBUMS WHERE ALBUMS.USER_ID=" + std::to_string(user.getId());
	return exec_albums_select(sqlStatement);
}

bool DataBaseAccess::open() 
{
	const int file_exists = _access(DATABASE_NAME, 0);
	const int res = sqlite3_open(DATABASE_NAME, &_db);

	if (res != SQLITE_OK)
	{
		_db = nullptr;
		std::cerr << "ERROR: Faild to create connection with " << DATABASE_NAME << ", CODE: " << res << std::endl;
		return false;
	}

	if (file_exists != 0)
		if (init_database(_db))
			return true;
		else
			return false;
	
    return true;
}

void DataBaseAccess::close()
{
    sqlite3_close(_db);
	_db = nullptr;
}

void DataBaseAccess::clear()
{
	const std::string sqlStatement = "DROP TABLE IF EXISTS TAGS;";
	const std::string sqlStatement2 = "DROP TABLE IF EXISTS PICTURES;";
	const std::string sqlStatement3 = "DROP TABLE IF EXISTS ALBUMS;";
	const std::string sqlStatement4 = "DROP TABLE IF EXISTS USERS; ";

	const std::string sqlStatements[] = { sqlStatement, sqlStatement2, sqlStatement3, sqlStatement4 };

	char* errMessage = nullptr;
	int res = 0;

	for (const std::string& command : sqlStatements)
	{
		res = sqlite3_exec(_db, command.c_str(), nullptr, nullptr, &errMessage);

		if (res != SQLITE_OK) {
			std::cerr << "ERROR: " << errMessage << std::endl;
			delete errMessage;
			close();
		}
	}

	init_database(_db);
}

void DataBaseAccess::deleteAlbum(const std::string& albumName, int userId)
{
	Album album = openAlbum(albumName);
	const std::list<Picture> pictures = album.getPictures();

	
	for (const Picture& picture : pictures)
		removePictureFromAlbumByName(albumName, picture.getName());
		
	
	const std::string sqlStatement = "DELETE FROM ALBUMS WHERE NAME=\"" + albumName + "\" AND USER_ID=" + std::to_string(userId) + ";";
	exec_sql_statement(sqlStatement);

	closeAlbum(album);
}

void DataBaseAccess::closeAlbum(Album& pAlbum)
{

}

void DataBaseAccess::printAlbums()
{
	const std::string sqlStatement = "SELECT * FROM ALBUMS;";
	const std::list<Album>& albums = exec_albums_select(sqlStatement);
	
	for(const Album& album :albums )
		std::cout << std::setw(5) << "* " << album;
}

bool DataBaseAccess::init_database(sqlite3* db)
{
	const std::string userSqlStatement = "CREATE TABLE USERS (ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, NAME TEXT NOT NULL);";
	const std::string albumsSqlStatement = "CREATE TABLE ALBUMS (ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, NAME TEXT NOT NULL, USER_ID INTEGER NOT NULL, CREATION_DATE TEXT NOT NULL, FOREIGN KEY(USER_ID) REFERENCES USERS (ID));";
	const std::string picturesSqlStatement = "CREATE TABLE PICTURES ( ID  INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,  NAME TEXT  NOT NULL, LOCATION TEXT NOT NULL,CREATION_DATE TEXT NOT NULL, ALBUM_ID INTEGER NOT NULL, FOREIGN KEY(ALBUM_ID) REFERENCES ALBUMS (ID));";
	const std::string tagsSqlStatement = "CREATE TABLE TAGS(PICTURE_ID INTEGER NOT NULL, USER_ID INTEGER NOT NULL, PRIMARY KEY(PICTURE_ID,USER_ID),  FOREIGN KEY(PICTURE_ID ) REFERENCES PICTURES (ID), FOREIGN KEY(USER_ID ) REFERENCES USERS (ID));";

	const std::string sqlStatements[] = { userSqlStatement, albumsSqlStatement, picturesSqlStatement, tagsSqlStatement };

	char* errMessage = nullptr;
	int res = 0;

	for (const std::string& command : sqlStatements)
	{
		res = sqlite3_exec(db, command.c_str(), nullptr, nullptr, &errMessage);

		if (res != SQLITE_OK) {
			std::cerr << "ERROR: " << errMessage << std::endl;
			delete errMessage;
			close();
			return false;
		}
	}
	
	return true;
}

void DataBaseAccess::exec_sql_statement(const std::string& sqlStatement)
{
	char* errMessage = nullptr;

	const int res = sqlite3_exec(_db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);

	if (res != SQLITE_OK) {
		std::string temp = errMessage;
		delete errMessage;
		close();
		throw std::exception(temp.c_str());
	}
}

void DataBaseAccess::addPicturesToAlbum(Album& album)
{
	const std::string pictureSqlStatement = "SELECT * FROM PICTURES WHERE ALBUM_ID=(SELECT ID FROM ALBUMS WHERE NAME='" + album.getName() + "');";
	std::list<Picture> allPicturesInAlbums = std::list<Picture>();
	char* errMessage = nullptr;

	const int res = sqlite3_exec(_db, pictureSqlStatement.c_str(), pictureCallback, &allPicturesInAlbums, &errMessage);
	if (res != SQLITE_OK) {
		std::string temp = errMessage;

		delete errMessage;
		throw std::exception(temp.c_str());
	}

	for (Picture& picture : allPicturesInAlbums)
	{
		addTagsToPicture(picture);
		album.addPicture(picture);
	}
}

void DataBaseAccess::addTagsToPicture(Picture& picture)
{
	const std::string tagSqlStatement = "SELECT USER_ID FROM TAGS WHERE PICTURE_ID=" + std::to_string(picture.getId()) + ";";
	std::list<int> tagsId = std::list<int>();
	char* errMessage = nullptr;
	const int res = sqlite3_exec(_db, tagSqlStatement.c_str(), tagCallback, &tagsId, &errMessage);
	if (res != SQLITE_OK) {
		std::string temp = errMessage;

		delete errMessage;
		throw std::exception(temp.c_str());
	}
	for (const int& userId : tagsId)
		picture.tagUser(userId);
}

int albumCallback(void* data, int argc, char** argv, char** azColName)
{
	std::list<Album>* albums = (std::list<Album>*)data;
	Album temp(0, "", "");
	for (int i = 0; i < argc; i++) {
		
		if (std::string(azColName[i]) == "NAME") 
			temp.setName(argv[i]);
		
		else if (std::string(azColName[i]) == "USER_ID") 
			temp.setOwner(atoi(argv[i]));
		
		else if (std::string(azColName[i]) == "CREATION_DATE") 
			temp.setCreationDate(argv[i]);
	}
	albums->push_back(temp);
	return 0;
}

int userCallback(void* data, int argc, char** argv, char** azColName)
{
	std::list<User>* users = (std::list<User>*)data;
	User temp(0, "");
	for (int i = 0; i < argc; i++) {

		if (std::string(azColName[i]) == "NAME")
			temp.setName(argv[i]);

		else if (std::string(azColName[i]) == "ID")
			temp.setId(atoi(argv[i]));
	}
	users->push_back(temp);
	return 0;
}

int pictureCallback(void* data, int argc, char** argv, char** azColName)
{
	std::list<Picture>* pictures = (std::list<Picture>*)data;
	Picture temp(0, "");
	for (int i = 0; i < argc; i++) {

		if (std::string(azColName[i]) == "NAME")
			temp.setName(argv[i]);

		else if (std::string(azColName[i]) == "ID")
			temp.setId(atoi(argv[i]));
		else if (std::string(azColName[i]) == "LOCATION")
			temp.setPath(argv[i]);
		else if (std::string(azColName[i]) == "CREATION_DATE")
			temp.setCreationDate(azColName[i]);
	}
	pictures->push_back(temp);
	return 0;
}

int tagCallback(void* data, int argc, char** argv, char** azColName)
{
	std::list<int>* tags = (std::list<int>*)data;
	int id = 0;
	for (int i = 0; i < argc; i++) 
		if (std::string(azColName[i]) == "USER_ID")
			id = atoi(argv[i]);
	
	tags->push_back(id);
	return 0;
}

void DataBaseAccess::printUsers()
{
	const std::string sqlStatement = "SELECT * FROM USERS;";
	const std::list<User>& users = exec_users_select(sqlStatement);
	
	std::cout << "Users list:" << std::endl;
	std::cout << "-----------" << std::endl;
	for(const User& user : users)
		std::cout << user << std::endl;

}

User DataBaseAccess::getUser(int userId)
{
	const std::string sqlStatement = "SELECT * FROM USERS WHERE ID=" + std::to_string(userId) + ";";
	const std::list<User>& users = exec_users_select(sqlStatement);

	if (users.size() == 0)
		throw std::exception("ERROR: there is no user with the requried id\n");
	
	User user(users.front());
	return user;
}

void DataBaseAccess::createUser(User& user)
{
	const std::string sqlStatement = "INSERT INTO USERS (NAME) VALUES (\"" + user.getName() + "\");";
	exec_sql_statement(sqlStatement);
}

void DataBaseAccess::deleteUser(const User& user)
{
	const std::string sqlStatement = "DELETE FROM USERS WHERE NAME=\"" + user.getName() + "\" AND ID=" + std::to_string(user.getId()) + ";";
	exec_sql_statement(sqlStatement);
}

bool DataBaseAccess::doesUserExists(int userId)
{
	const std::string sqlStatement = "SELECT * FROM USERS WHERE ID=" + std::to_string(userId) + ";";
	const std::list<User>& users = exec_users_select(sqlStatement);

	return users.size() != 0;
}

int DataBaseAccess::countAlbumsOwnedOfUser(const User& user)
{
	const std::string sqlStatement = "SELECT * FROM ALBUMS WHERE USER_ID=" + std::to_string(user.getId()) + ";";
	const std::list<Album> allAlbums = exec_albums_select(sqlStatement);

	return allAlbums.size();;
}

int DataBaseAccess::countAlbumsTaggedOfUser(const User& user)
{
	const std::list<Album> albums = getAlbums();
	int counter = 0;

	for (const Album& album : albums)
	{
		const std::list<Picture> pictures = album.getPictures();
		for (const Picture& picture : pictures)
			if (picture.isUserTagged(user)) {
				counter++;
				break;
			}
	}
	return counter;
}

int DataBaseAccess::countTagsOfUser(const User& user)
{
	int tagsCount = 0;
	const std::list<Album> albums = getAlbums();

	for (const auto& album : albums) {
		const std::list<Picture>& pics = album.getPictures();

		for (const auto& picture : pics) 
			if (picture.isUserTagged(user)) 
				tagsCount++;
	}
	return tagsCount;
}

float DataBaseAccess::averageTagsPerAlbumOfUser(const User& user)
{
	const int albumsTaggedCount = countAlbumsTaggedOfUser(user);

	if (0 == albumsTaggedCount) {
		return 0;
	}

	return static_cast<float>(countTagsOfUser(user)) / albumsTaggedCount;
}

User DataBaseAccess::getTopTaggedUser()
{
	std::map<int, int> userTagsCountMap;
	const std::list<Album> albums = getAlbums();

	for (const auto& album : albums) {
		for (const auto& picture : album.getPictures()) {

			const std::set<int>& userTags = picture.getUserTags();
			for (const auto& user : userTags) {
				//As map creates default constructed values, 
				//users which we haven't yet encountered will start from 0
				userTagsCountMap[user]++;
			}
		}
	}

	if (userTagsCountMap.size() == 0) {
		throw std::exception("There isn't any tagged user.");
	}

	int topTaggedUser = -1;
	int currentMax = -1;
	for (auto entry : userTagsCountMap) {
		if (entry.second < currentMax) 
			continue;

		topTaggedUser = entry.first;
		currentMax = entry.second;
	}

	if (-1 == topTaggedUser) 
		throw std::exception("Failed to find most tagged user");
	
	return getUser(topTaggedUser);
}

Picture DataBaseAccess::getTopTaggedPicture()
{
	int currentMax = -1;
	const Picture* mostTaggedPic = nullptr;
	std::list<Album> albums = getAlbums();

	for (const auto& album : albums) {
		for (const Picture& picture : album.getPictures()) {
			int tagsCount = picture.getTagsCount();
			if (tagsCount == 0) 
				continue;

			if (tagsCount <= currentMax) 
				continue;

			mostTaggedPic = &picture;
			currentMax = tagsCount;
		}
	}
	if (nullptr == mostTaggedPic) {
		throw std::exception("There isn't any tagged picture.");
	}
	return *mostTaggedPic;
}

std::list<Picture> DataBaseAccess::getTaggedPicturesOfUser(const User& user)
{
	std::list<Picture> pictures;
	const std::list<Album> albums = getAlbums();

	for (const auto& album : albums) 
		for (const auto& picture : album.getPictures()) 
			if (picture.isUserTagged(user)) 
				pictures.push_back(picture);
	
	return pictures;
}

void DataBaseAccess::addPictureToAlbumByName(const std::string& albumName, const Picture& picture)
{
	const std::string sqlStatement = "INSERT INTO PICTURES (NAME, CREATION_DATE, LOCATION, ALBUM_ID) VALUES ('" +
		picture.getName() + "','" + picture.getCreationDate() +
		"','" + picture.getPath() + "', (SELECT ID FROM ALBUMS WHERE NAME='" + albumName + "'));";
	exec_sql_statement(sqlStatement);
}

void DataBaseAccess::removePictureFromAlbumByName(const std::string& albumName, const std::string& pictureName)
{
	const std::string sqlStatement = "DELETE FROM PICTURES WHERE NAME='" + pictureName +
		"' AND ALBUM_ID=(SELECT ID FROM ALBUMS WHERE NAME='" + albumName + "');";
	exec_sql_statement(sqlStatement);
}

void DataBaseAccess::tagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId)
{	
	const std::string sqlStatement = "INSERT INTO TAGS (PICTURE_ID, USER_ID) VALUES"
		" ((SELECT ID FROM PICTURES WHERE NAME = '" + pictureName + 
		"' AND ALBUM_ID = (SELECT ID FROM ALBUMS WHERE NAME = '" + albumName + "')), " + 
		std::to_string(userId) + ")";

	exec_sql_statement(sqlStatement);
}

void DataBaseAccess::untagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId)
{
	const std::string sqlStatement = "DELETE FROM TAGS WHERE USER_ID=" +
		std::to_string(userId) + 
		" AND PICTURE_ID=(SELECT ID FROM PICTURES WHERE NAME=\"" + pictureName +
		"\" AND ALBUM_ID="
		"(SELECT ID FROM ALBUMS WHERE NAME=\"" + albumName + "\"));";

	exec_sql_statement(sqlStatement);
}
