#include <iostream>
#include <string>
#include "MemoryAccess.h"
#include "DataBaseAccess.h"
#include "AlbumManager.h"
#include <ctime>
#include <chrono>


int getCommandNumberFromUser()
{
	std::string message("\nPlease enter any command(use number): ");
	std::string numericStr("0123456789");
	
	std::cout << message << std::endl;
	std::string input;
	std::getline(std::cin, input);
	
	while (std::cin.fail() || std::cin.eof() || input.find_first_not_of(numericStr) != std::string::npos) {

		std::cout << "Please enter a number only!" << std::endl;

		if (input.find_first_not_of(numericStr) == std::string::npos) {
			std::cin.clear();
		}

		std::cout << std::endl << message << std::endl;
		std::getline(std::cin, input);
	}
	
	return std::atoi(input.c_str());
}


void printSystemDetails()
{
	std::time_t now = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());

	std::string s(30, '\0');
	std::strftime(&s[0], s.size(), "%Y-%m-%d %H:%M:%S", std::localtime(&now));
	std::cout << "Ori Lev  " << s << std::endl;
}


int main(void)
{
	// initialization data access
	//MemoryAccess dataAccess;

	// initialization database access
	DataBaseAccess databaseAccess;
	// initialize album manager
	AlbumManager albumManager(databaseAccess);
	
	printSystemDetails();

	std::string albumName;
	std::cout << "Welcome to Gallery!" << std::endl;
	std::cout << "===================" << std::endl;
	std::cout << "Type " << HELP << " to a list of all supported commands" << std::endl;
	
	do {
		int commandNumber = getCommandNumberFromUser();
		
		try	{
			albumManager.executeCommand(static_cast<CommandType>(commandNumber));
		} catch (const std::exception& e) {	
			std::cout << e.what() << std::endl;
		}
	} 
	while (true);
}


